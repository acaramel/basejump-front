import { Link } from "react-router-dom";

const Nav = () => {
    return(
        <nav>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/About">About</Link>
                </li>
                <li>
                    <Link to="/connexion">Connexion</Link>
                </li>
                <li>
                    <Link to="/mon-compte">Mon compte</Link>
                </li>
            </ul>
        </nav>
    )
}

export default Nav;