import Footer from "../components/Footer";
import Nav from "../components/Nav";

const Home = () => {
    return (
        <section id="home">

            <h1>Home</h1>
            <Nav></Nav>
            <Footer></Footer>
        </section>
    )
}

export default Home;