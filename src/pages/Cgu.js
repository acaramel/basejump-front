import Footer from "../components/Footer";
import Nav from "../components/Nav";

const CGU = () => {
    return (
        <section id="CGU">
            <h1>CGU</h1>
            <Nav/>
            <Footer/>
        </section>
    )
}

export default CGU;