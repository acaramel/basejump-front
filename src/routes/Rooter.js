import { BrowserRouter as Router,Switch,Route} from "react-router-dom";
import NotFound from '../pages/NotFound.js';
import Home from "../pages/Home.js";
import Connexion from "../pages/connexion.js";
import CGU from "../pages/Cgu.js";


const Rooter = () => (
    <Router>
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/connexion" component={Connexion}/>
            <Route exact path="/cgu" component={CGU}/>
            <Route path="*" component={NotFound}/>
        </Switch>
    </Router>
);

export default Rooter;